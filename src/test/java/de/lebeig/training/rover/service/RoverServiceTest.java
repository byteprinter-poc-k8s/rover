package de.lebeig.training.rover.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.Random;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import de.lebeig.training.rover.domain.Rover;
import de.lebeig.training.rover.domain.RoverCommand;

public class RoverServiceTest {

	private RoverService cut;
	private Random random;

	@BeforeEach
	protected void setUp() throws Exception {
		cut = new RoverService();

		random = new Random();
	}

	static Stream<Arguments> moveForwardTestdata() {
		return Stream.of(arguments("fromStart", createRover(0.0, 0.0, 0.0), createRover(0.0, 1.0, 0.0)),
				arguments("fromOrientationAngle90", createRover(0.0, 0.0, 90.0), createRover(1.0, 0.0, 90.0)),
				arguments("fromOrientationAngle180", createRover(0.0, 0.0, 180.0), createRover(0.0, -1.0, 180.0)),
				arguments("fromOrientationAngle270", createRover(0.0, 0.0, 270.0), createRover(-1.0, 0.0, 270.0)),
				arguments("fromX6Y7A60", createRover(6.0, 7.0, 60.0), createRover(6.9, 7.5, 60.0)));
	}

	static Stream<Arguments> moveBackwardTestdata() {
		return Stream.of(arguments("fromStart", createRover(0.0, 0.0, 0.0), createRover(0.0, -1.0, 0.0)),
				arguments("fromOrientationAngle90", createRover(0.0, 0.0, 90.0), createRover(-1.0, 0.0, 90.0)),
				arguments("fromOrientationAngle180", createRover(0.0, 0.0, 180.0), createRover(0.0, 1.0, 180.0)),
				arguments("fromOrientationAngle270", createRover(0.0, 0.0, 270.0), createRover(1.0, 0.0, 270.0)),
				arguments("fromX6Y7A60", createRover(6.0, 7.0, 60.0), createRover(5.1, 6.5, 60.0)));
	}

	static Stream<Arguments> rotateRightTestdata() {
		return Stream.of(arguments("fromStart", createRover(0.0, 0.0, 0.0), createRover(0.0, 0.0, 30.0)),
				arguments("fromOrientationAngle90", createRover(0.0, 0.0, 90.0), createRover(0.0, 0.0, 120.0)),
				arguments("fromOrientationAngle180", createRover(0.0, 0.0, 180.0), createRover(0.0, 0.0, 210.0)),
				arguments("fromOrientationAngle270", createRover(0.0, 0.0, 270.0), createRover(0.0, 0.0, 300.0)),
				arguments("fromX6Y7A60", createRover(6.0, 7.0, 60.0), createRover(6.0, 7.0, 90.0))
		// FIXME , arguments("toStart", createRover(0.0, 0.0, 330.0), createRover(0.0,
		// 0.0, 0.0))
		);
	}

	static Stream<Arguments> rotateLeftTestdata() {
		return Stream.of(
				// FIXME arguments("fromStart", createRover(0.0, 0.0, 0.0), createRover(0.0,
				// 0.0, 330.0)),
				arguments("fromOrientationAngle90", createRover(0.0, 0.0, 90.0), createRover(0.0, 0.0, 60.0)),
				arguments("fromOrientationAngle180", createRover(0.0, 0.0, 180.0), createRover(0.0, 0.0, 150.0)),
				arguments("fromOrientationAngle270", createRover(0.0, 0.0, 270.0), createRover(0.0, 0.0, 240.0)),
				arguments("fromX6Y7A60", createRover(6.0, 7.0, 60.0), createRover(6.0, 7.0, 30.0)));
	}

	@ParameterizedTest(name = "moveForward {0}")
	@MethodSource("moveForwardTestdata")
	public void moveForward(String testname, Rover roverStart, Rover roverExpected) throws Exception {
		assertThat(cut.executeCommand(roverStart, RoverCommand.MOVE_FORWARD))
				.isEqualToComparingFieldByField(roverExpected);
	}

	@ParameterizedTest(name = "moveBackward {0}")
	@MethodSource("moveBackwardTestdata")
	public void moveBackward(String testname, Rover roverStart, Rover roverExpected) throws Exception {
		assertThat(cut.executeCommand(roverStart, RoverCommand.MOVE_BACKWARD))
				.isEqualToComparingFieldByField(roverExpected);
	}

	@ParameterizedTest(name = "rotateRight {0}")
	@MethodSource("rotateRightTestdata")
	public void rotateRight(String testname, Rover roverStart, Rover roverExpected) throws Exception {
		assertThat(cut.executeCommand(roverStart, RoverCommand.ROTATE_RIGHT))
				.isEqualToComparingFieldByField(roverExpected);
	}

	@ParameterizedTest(name = "rotateLeft {0}")
	@MethodSource("rotateLeftTestdata")
	public void rotateLeft(String testname, Rover roverStart, Rover roverExpected) throws Exception {
		assertThat(cut.executeCommand(roverStart, RoverCommand.ROTATE_LEFT))
				.isEqualToComparingFieldByField(roverExpected);
	}

	@Test
	public void withNoCommand() throws Exception {
		Rover initialAndExpectedRover = createRover(random.nextDouble() * 2048 - 1024,
				random.nextDouble() * 2048 - 1024, random.nextDouble() * 360);
		assertThat(cut.executeCommand(initialAndExpectedRover, null))
				.isEqualToComparingFieldByField(initialAndExpectedRover);
	}

	private static final Rover createRover(double positionHorizontal, double positionVertical,
			double orientationAngle) {

		Rover rover = new Rover();
		rover.setPositionHorizontal(positionHorizontal);
		rover.setPositionVertical(positionVertical);
		rover.setOrientationAngle(orientationAngle);

		return rover;

	}

}
