FROM openjdk:8-jdk-alpine
WORKDIR /application
EXPOSE 8080
ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /application/lib
COPY ${DEPENDENCY}/META-INF /application/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /application
CMD ["java","-cp",".:lib/*","de.lebeig.training.rover.App"]